from django.shortcuts import render
from .models import TodoList

# Create your views here.


def todo_list(request):
    list_of_todo = TodoList.objects.all()
    context = {
        "todo": list_of_todo,
    }
    return render(request, "list.html", context)
